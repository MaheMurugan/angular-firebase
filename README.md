
Why Firebase?
	Firebase is based on a data structure used by the NoSQL database is vastly different from those used in a relational database. Some operations are faster in NoSQL than relational databases like MySQL. Data structures used by NoSQL databases can also be viewed as more flexible and scalable than relational databases.

Step 1. Created a account in firebase
Step 2. Created DB in the name of Task
Step 3. Added field as 1. Title 2. Duration 3. StartDate 4. EndDate 5. Remarks
Step 4. Created Environment for front-end which access Firebase as a Back-End (From the scrach)
Step 5. Create Index.html
	<html>

<head>
    <link rel="stylesheet" href="styles.css">
    <script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-firestore.js"></script>
</head>

<body>
    <h1>Time Sheet</h1>
    <div>
        <form id="add-time-sheet-form">
          
        </form>
        <ul id="time-sheet-list"></ul>

    </div>

    <!-- The core Firebase JS SDK is always required and must be listed first -->

    <!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->

    <script>
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyDGXxD5YhpEpn9yBK10Xaa3E3OIB2ade8Q",
            authDomain: "angularbot-47075.firebaseapp.com",
            databaseURL: "https://angularbot-47075.firebaseio.com",
            projectId: "angularbot-47075",
            storageBucket: "angularbot-47075.appspot.com",
            messagingSenderId: "525867594640",
            appId: "1:525867594640:web:d9be2f957144d7e89b3915"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        const db = firebase.firestore();
       // db.settings({ timestampInSnapshots: true });
    </script>
    <script src="app.js"></script>

</body>

</html>
Step 6. Create styles.css

Step 7. Create app.js
		const timeSheetList = document.querySelector('#time-sheet-list');

function renderTimeSheet(task) {
    let li = document.createElement('li');
    let Title = document.createElement('span');
    let Duration = document.createElement('span');
    let StartDate = document.createElement('span');
    let EndDate = document.createElement('span');
    let Remark = document.createElement('span');

    li.setAttribute('data-id', task.id);
    EndDate.textContent = task.data().EndDate;
    Title.textContent = task.data().Title;
    Duration.textContent = task.data().Duration;
    StartDate.textContent = task.data().StartDate;
    Remark.textContent = task.data().Remark;

    li.append(EndDate);
    li.append(Title);
    li.append(Duration);
    li.append(StartDate);
    li.append(Remark);
    timeSheetList.append(li);  

}


db.collection('Task').get().then((snapshot) => {
    snapshot.docs.forEach(element => {
        renderTimeSheet(element);
    });

}
)
Step 8. Installed LiveServer from Extension

Step 9. Added Config information from firebase console and paste it in the index.html
		 var firebaseConfig = {
            apiKey: "AIzaSyDGXxD5YhpEpn9yBK10Xaa3E3OIB2ade8Q",
            authDomain: "angularbot-47075.firebaseapp.com",
            databaseURL: "https://angularbot-47075.firebaseio.com",
            projectId: "angularbot-47075",
            storageBucket: "angularbot-47075.appspot.com",
            messagingSenderId: "525867594640",
            appId: "1:525867594640:web:d9be2f957144d7e89b3915"
        };
Step 10. Added URL for calling api service for Db calling.
	    <script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-firestore.js"></script>
	
Step 11. Get task list from firebase using promise call